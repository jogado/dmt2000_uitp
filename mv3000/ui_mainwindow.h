/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *MainScreen;
    QLabel *Counter;
    QLabel *free_text;
    QLabel *Head;
    QLabel *ip_address;
    QWidget *CardScreen;
    QWidget *SplashScreen;
    QWidget *BarcodeScreen;
    QWidget *AboutScreen;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(320, 240);
        MainWindow->setStyleSheet(QStringLiteral("background-color:black"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral("color:black;"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 320, 240));
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QStringLiteral(""));
        MainScreen = new QWidget();
        MainScreen->setObjectName(QStringLiteral("MainScreen"));
        MainScreen->setEnabled(true);
        MainScreen->setStyleSheet(QLatin1String("background-image:url(:/jpg/BG2_320x240.jpg);\n"
"background-position: center;"));
        Counter = new QLabel(MainScreen);
        Counter->setObjectName(QStringLiteral("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(40, 160, 100, 20));
        QFont font;
        font.setFamily(QStringLiteral("Liberation Sans"));
        font.setPointSize(12);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        Counter->setFont(font);
        Counter->setStyleSheet(QLatin1String("color:black;\n"
"background-color:white;\n"
"\n"
"\n"
""));
        Counter->setAlignment(Qt::AlignCenter);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QStringLiteral("free_text"));
        free_text->setGeometry(QRect(40, 81, 240, 60));
        QFont font1;
        font1.setFamily(QStringLiteral("Liberation Sans"));
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        free_text->setFont(font1);
        free_text->setStyleSheet(QLatin1String("color:black;\n"
"WA_TranslucentBackground:1;\n"
"\n"
""));
        free_text->setAlignment(Qt::AlignCenter);
        free_text->setWordWrap(true);
        Head = new QLabel(MainScreen);
        Head->setObjectName(QStringLiteral("Head"));
        Head->setGeometry(QRect(40, 43, 240, 20));
        QFont font2;
        font2.setFamily(QStringLiteral("Liberation Sans"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        Head->setFont(font2);
        Head->setStyleSheet(QLatin1String("color:black;\n"
"background-color:white;"));
        Head->setScaledContents(false);
        Head->setAlignment(Qt::AlignCenter);
        ip_address = new QLabel(MainScreen);
        ip_address->setObjectName(QStringLiteral("ip_address"));
        ip_address->setEnabled(true);
        ip_address->setGeometry(QRect(160, 160, 120, 20));
        ip_address->setFont(font);
        ip_address->setStyleSheet(QLatin1String("color:black;\n"
"background-color:white;\n"
"\n"
"\n"
""));
        ip_address->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        stackedWidget->addWidget(MainScreen);
        CardScreen = new QWidget();
        CardScreen->setObjectName(QStringLiteral("CardScreen"));
        CardScreen->setEnabled(true);
        stackedWidget->addWidget(CardScreen);
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QStringLiteral("SplashScreen"));
        SplashScreen->setEnabled(true);
        SplashScreen->setStyleSheet(QStringLiteral("image:url(:/jpg/BG3_320x240.jpg)"));
        stackedWidget->addWidget(SplashScreen);
        BarcodeScreen = new QWidget();
        BarcodeScreen->setObjectName(QStringLiteral("BarcodeScreen"));
        stackedWidget->addWidget(BarcodeScreen);
        AboutScreen = new QWidget();
        AboutScreen->setObjectName(QStringLiteral("AboutScreen"));
        stackedWidget->addWidget(AboutScreen);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        Counter->setText(QApplication::translate("MainWindow", "----", 0));
        free_text->setText(QApplication::translate("MainWindow", "-----", 0));
        Head->setText(QApplication::translate("MainWindow", "Present Card / Barcode", 0));
        ip_address->setText(QApplication::translate("MainWindow", "----", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
