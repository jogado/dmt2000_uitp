#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>


#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";




#define MAIN_SCREEN       0
#define CARD_SCREEN       1
#define SPLASH_SCREEN     2
#define BARCODE_SCREEN    3
#define ABOUT_SCREEN      4

int CurrentScreen = SPLASH_SCREEN;
int DelayTimer = 20;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);


    ui->Counter->setVisible(true);


    /*
    ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
    ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
    ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );
    */
    //ui->SplashScreen->setStyleSheet  ( "background-image:url(\"./BG3_320x240.jpg\"); background-position: center;" );
    //ui->MainScreen->setStyleSheet    ( "background-image:url(\"./BG2_320x240.jpg\"); background-position: center;" );

    ui->Counter->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->free_text->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->Head->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->ip_address->setAttribute(Qt::WA_TranslucentBackground,true);



    ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
    CurrentScreen = SPLASH_SCREEN;
    DelayTimer = 20;

    system("ifconfig eth0 |grep 'inet addr'  |cut -c 21-34 > /tmp/ip.txt");
    system("echo \"-----\" > text.txt");
    system("echo \"\" > moti.tmp");
    system("echo \"\" > rakinda.tmp");


    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

    }

MainWindow::~MainWindow()
{
    delete ui;
}



int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}






int loop=0;
int read_ip=0;
void MainWindow::MainTask_loop()
{

    QString res = "reset";

    loop++;
    Label2.sprintf("%09d",loop/10);
    ui->Counter->setText(Label2);


    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != MAIN_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = MAIN_SCREEN ;
    }

    //=============================================================================

    read_barcode("rakinda.tmp");

    if(aTmp_barco.length()<3)
    {
       // loop=0;
       // ui->free_text->setText(aTmp_barco);
    }

    if(aTmp_barco.length()>2)
    {
        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
        system("beep");
        system("echo \"\" > rakinda.tmp");
    }


    //=============================================================================
    if( read_ip == 0)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }

    if( loop%10==3)
    {
        system("ifconfig eth0 |grep 'inet addr'  |cut -c 21-34 > /tmp/ip.txt");
    }
    if( loop%10==5)
    {
        read("/tmp/ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
        }
        else
        {
            ui->ip_address->setText("-----");
        }
     }



    //=============================================================================
    read("moti.tmp");
    if(aTmp.length()>5)
    {

        ui->free_text->setText(aTmp);
        QCoreApplication::processEvents();
        system("beep");
        system("echo \"\" > moti.tmp");
        QCoreApplication::processEvents();
    }
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}
