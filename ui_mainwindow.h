/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *MainScreen;
    QLabel *time_date;
    QPushButton *bt_5;
    QLabel *time_text;
    QLabel *free_text;
    QFrame *frame;
    QLabel *ip_address;
    QLabel *label;
    QLabel *label_2;
    QLabel *Counter;
    QPushButton *bt_4;
    QPushButton *bt_3;
    QPushButton *bt_2;
    QLabel *free_text_2;
    QPushButton *bt_1;
    QTextBrowser *textBrowser;
    QPushButton *bt_6;
    QWidget *CardScreen;
    QWidget *SplashScreen;
    QWidget *BarcodeScreen;
    QWidget *AboutScreen;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 480);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 800, 480));
        QFont font;
        font.setPointSize(24);
        font.setBold(true);
        font.setWeight(75);
        stackedWidget->setFont(font);
        stackedWidget->setCursor(QCursor(Qt::BlankCursor));
        stackedWidget->setStyleSheet(QStringLiteral(""));
        MainScreen = new QWidget();
        MainScreen->setObjectName(QStringLiteral("MainScreen"));
        MainScreen->setEnabled(true);
        time_date = new QLabel(MainScreen);
        time_date->setObjectName(QStringLiteral("time_date"));
        time_date->setGeometry(QRect(370, 410, 111, 16));
        QFont font1;
        font1.setFamily(QStringLiteral("Liberation Sans"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        time_date->setFont(font1);
        time_date->setStyleSheet(QLatin1String("color:red\n"
""));
        time_date->setAlignment(Qt::AlignCenter);
        bt_5 = new QPushButton(MainScreen);
        bt_5->setObjectName(QStringLiteral("bt_5"));
        bt_5->setEnabled(true);
        bt_5->setGeometry(QRect(6, 344, 171, 60));
        QFont font2;
        font2.setFamily(QStringLiteral("Liberation Mono"));
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setWeight(75);
        bt_5->setFont(font2);
        bt_5->setStyleSheet(QStringLiteral("color:black"));
        time_text = new QLabel(MainScreen);
        time_text->setObjectName(QStringLiteral("time_text"));
        time_text->setGeometry(QRect(530, 100, 101, 20));
        QFont font3;
        font3.setFamily(QStringLiteral("Liberation Sans"));
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setWeight(75);
        time_text->setFont(font3);
        time_text->setStyleSheet(QLatin1String("color:yellow\n"
""));
        time_text->setAlignment(Qt::AlignCenter);
        free_text = new QLabel(MainScreen);
        free_text->setObjectName(QStringLiteral("free_text"));
        free_text->setGeometry(QRect(10, 150, 800, 81));
        QFont font4;
        font4.setFamily(QStringLiteral("Liberation Sans"));
        font4.setPointSize(28);
        font4.setBold(true);
        font4.setWeight(75);
        free_text->setFont(font4);
        free_text->setStyleSheet(QLatin1String("color:yellow\n"
""));
        free_text->setAlignment(Qt::AlignCenter);
        frame = new QFrame(MainScreen);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(0, -2, 800, 60));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        ip_address = new QLabel(frame);
        ip_address->setObjectName(QStringLiteral("ip_address"));
        ip_address->setGeometry(QRect(621, 26, 131, 16));
        ip_address->setFont(font1);
        ip_address->setStyleSheet(QStringLiteral("color:blue"));
        ip_address->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(536, 22, 81, 20));
        QFont font5;
        font5.setFamily(QStringLiteral("Liberation Sans"));
        font5.setPointSize(12);
        font5.setItalic(false);
        label->setFont(font5);
        label->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(340, 22, 81, 20));
        label_2->setFont(font5);
        label_2->setAlignment(Qt::AlignCenter);
        Counter = new QLabel(frame);
        Counter->setObjectName(QStringLiteral("Counter"));
        Counter->setEnabled(true);
        Counter->setGeometry(QRect(426, 26, 91, 16));
        Counter->setFont(font1);
        Counter->setStyleSheet(QStringLiteral("color:blue"));
        Counter->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        time_text->raise();
        time_date->raise();
        ip_address->raise();
        label->raise();
        label_2->raise();
        Counter->raise();
        bt_4 = new QPushButton(MainScreen);
        bt_4->setObjectName(QStringLiteral("bt_4"));
        bt_4->setEnabled(true);
        bt_4->setGeometry(QRect(6, 274, 171, 60));
        bt_4->setFont(font2);
        bt_4->setStyleSheet(QStringLiteral("color:black"));
        bt_3 = new QPushButton(MainScreen);
        bt_3->setObjectName(QStringLiteral("bt_3"));
        bt_3->setEnabled(true);
        bt_3->setGeometry(QRect(6, 204, 171, 60));
        bt_3->setFont(font2);
        bt_3->setStyleSheet(QStringLiteral("color:black"));
        bt_2 = new QPushButton(MainScreen);
        bt_2->setObjectName(QStringLiteral("bt_2"));
        bt_2->setEnabled(true);
        bt_2->setGeometry(QRect(6, 133, 171, 60));
        bt_2->setFont(font2);
        bt_2->setStyleSheet(QStringLiteral("color:black"));
        free_text_2 = new QLabel(MainScreen);
        free_text_2->setObjectName(QStringLiteral("free_text_2"));
        free_text_2->setGeometry(QRect(420, 360, 800, 81));
        free_text_2->setFont(font4);
        free_text_2->setStyleSheet(QLatin1String("color:yellow\n"
""));
        free_text_2->setAlignment(Qt::AlignCenter);
        bt_1 = new QPushButton(MainScreen);
        bt_1->setObjectName(QStringLiteral("bt_1"));
        bt_1->setEnabled(true);
        bt_1->setGeometry(QRect(6, 63, 171, 60));
        bt_1->setFont(font2);
        bt_1->setStyleSheet(QLatin1String("color:black\n"
""));
        textBrowser = new QTextBrowser(MainScreen);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(185, 64, 610, 411));
        bt_6 = new QPushButton(MainScreen);
        bt_6->setObjectName(QStringLiteral("bt_6"));
        bt_6->setEnabled(true);
        bt_6->setGeometry(QRect(6, 414, 171, 60));
        bt_6->setFont(font2);
        bt_6->setStyleSheet(QStringLiteral("color:black"));
        stackedWidget->addWidget(MainScreen);
        CardScreen = new QWidget();
        CardScreen->setObjectName(QStringLiteral("CardScreen"));
        CardScreen->setEnabled(true);
        stackedWidget->addWidget(CardScreen);
        SplashScreen = new QWidget();
        SplashScreen->setObjectName(QStringLiteral("SplashScreen"));
        SplashScreen->setEnabled(true);
        stackedWidget->addWidget(SplashScreen);
        BarcodeScreen = new QWidget();
        BarcodeScreen->setObjectName(QStringLiteral("BarcodeScreen"));
        stackedWidget->addWidget(BarcodeScreen);
        AboutScreen = new QWidget();
        AboutScreen->setObjectName(QStringLiteral("AboutScreen"));
        stackedWidget->addWidget(AboutScreen);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        time_date->setText(QApplication::translate("MainWindow", "00:00:00", Q_NULLPTR));
        bt_5->setText(QApplication::translate("MainWindow", "Light", Q_NULLPTR));
        time_text->setText(QApplication::translate("MainWindow", "00:00:00", Q_NULLPTR));
        free_text->setText(QApplication::translate("MainWindow", "---", Q_NULLPTR));
        ip_address->setText(QApplication::translate("MainWindow", "000.000.000.000", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Network", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Alive", Q_NULLPTR));
        Counter->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        bt_4->setText(QApplication::translate("MainWindow", "Heat", Q_NULLPTR));
        bt_3->setText(QApplication::translate("MainWindow", "Doors", Q_NULLPTR));
        bt_2->setText(QApplication::translate("MainWindow", "Status", Q_NULLPTR));
        free_text_2->setText(QApplication::translate("MainWindow", "---", Q_NULLPTR));
        bt_1->setText(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
        bt_6->setText(QApplication::translate("MainWindow", "Micro", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
