#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QDateTime>


#include <QTimer>

QString aTmp;
QString aTmp_barco;
QString aTitle;
QString MyTime;

char aBarco[] = "Barcode input";




#define MAIN_SCREEN       0
#define CARD_SCREEN       1
#define SPLASH_SCREEN     2
#define BARCODE_SCREEN    3
#define ABOUT_SCREEN      4

int CurrentScreen = MAIN_SCREEN;
int DelayTimer = 10;
QString Label2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);


    ui->Counter->setVisible(true);


    ui->time_date->setVisible(false);
    ui->time_text->setVisible(false);
    //ui->Exit_button->setVisible(false);



   // ui->BarcodeScreen->setStyleSheet ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->CardScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/neutralscreen.jpg\"); background-position: center;" );
   // ui->MainScreen->setStyleSheet    ( "background-image:url(\"/etc/demo/jpg/mainscreen.jpg\"); background-position: center;" );
   // ui->SplashScreen->setStyleSheet  ( "background-image:url(\"/etc/demo/jpg/splashscreen.jpg\"); background-position: center;" );
   // ui->AboutScreen->setStyleSheet   ( "background-image:url(\"/etc/demo/jpg/aboutscreen.jpg\"); background-position: center;" );

   // ui->stackedWidget->setCurrentIndex(SPLASH_SCREEN);
   // CurrentScreen = SPLASH_SCREEN;
   // DelayTimer = 50;

    ui->stackedWidget->setCurrentIndex(MAIN_SCREEN);
    CurrentScreen = MAIN_SCREEN;



    system("ifconfig eth0 |grep 'inet addr' |cut -c 21-34 > ip.txt");
     system("echo \"-----\" > text.txt");

    //============================================================================
    // MainTask (100 ms)
    //============================================================================
    QTimer *MainTask = new QTimer(this);
    connect(MainTask, SIGNAL(timeout()), this, SLOT(MainTask_loop()));
    MainTask->start(100);

    // system("./mute.sh");
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*
void MainWindow::on_pushButton_beep_clicked()
{
    system("beep");
    ui->stackedWidget->setCurrentIndex(0);


}

void MainWindow::on_pushButton_audio_clicked()
{
    system("amixer sset 'PCM' 100%");
    system("aplay /usr/share/audio/22294.wav");
}
*/



int FileExist(QString filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        file.close();
        return (0);
    }
    file.close();
    return(1);
}




void read(QString filename)
{
    QFile file(filename);
    QTextStream in(&file);

    if(!file.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file.close();
        return;
    }

    aTmp.clear();
    aTmp = in.readAll();
    file.close();
}

void read_barcode(QString filename)
{
    QFile file_barco(filename);
    QTextStream inbarco(&file_barco);

    if(!file_barco.open(QFile::ReadWrite  |
                  QFile::Text))
    {
        file_barco.close();
        return;
    }

    aTmp_barco.clear();
    aTmp_barco = inbarco.readAll();
    file_barco.close();
}






int loop=0;
int read_ip=0;
void MainWindow::MainTask_loop()
{


//   QString text = "Hour:" + QTime::currentTime().toString();

    QDateTime current = QDateTime::currentDateTime();
    QDateTime Old     = QDateTime::currentDateTime();
    QString TheTime = current.toString("hh:mm:ss");
    QString TheDate = current.toString("dd/MM/yyyy");
    QString res = "reset";

    loop++;
    Label2.sprintf("%09d",loop/10);
    ui->Counter->setText(Label2);

    ui->time_text->setText(TheTime);
    ui->time_date->setText(TheDate);

    if( DelayTimer) DelayTimer--;

    if( (DelayTimer == 0) && (CurrentScreen != MAIN_SCREEN))
    {
       ui->stackedWidget->setCurrentIndex(0);
       CurrentScreen = MAIN_SCREEN ;
    }



    if(loop%600 == 0)
    {
        system("echo O >  /sys/class/graphics/fb0/blank");
    }



    //=============================================================================
    read_barcode("text.txt");

    if(aTmp_barco.length()<3)
    {
        loop=0;
        ui->free_text->setText(aTmp_barco);
    }

    if(aTmp_barco.length()>2)
    {
        ui->free_text->setText(aTmp_barco);
        QCoreApplication::processEvents();
    }
    //=============================================================================
    if( read_ip == 0)
    {
        read("ip.txt");
        if(aTmp.length()>2)
        {
            ui->ip_address->setText(aTmp);
            read_ip=1;
        }
     }
    /*
    read("moti.tmp");
    if(aTmp.length()>5)
    {
        DelayTimer = 30 ;
        if(CurrentScreen != CARD_SCREEN )
        {
            ui->stackedWidget->setCurrentIndex(CARD_SCREEN);
        }
        CurrentScreen = CARD_SCREEN ;

        ui->Card_text->setText(aTmp);

        system("dobeep.sh &");

        if( FileExist("moti.tmp") )
        {
           system("echo \"---\" > moti.tmp");
        }
        QCoreApplication::processEvents();
    }
    */
    //=============================================================================
    /*
    read_barcode("rakinda.tmp");
    if(aTmp_barco.length()>5)
    {
        DelayTimer = 30 ;
        if(CurrentScreen != BARCODE_SCREEN )
        {
            ui->stackedWidget->setCurrentIndex(BARCODE_SCREEN);
        }
        CurrentScreen = BARCODE_SCREEN ;

        ui->Barcode_text->setText(aTmp_barco);

        system("dobeep.sh &");

        if( FileExist("rakinda.tmp") )
        {
            system("echo \"\" > rakinda.tmp");
        }
        QCoreApplication::processEvents();
    }
    */
    //=============================================================================
    QCoreApplication::processEvents();
}




void MainWindow::MasterTimer_update()
{
    static int count=0;
    count++;
    QCoreApplication::processEvents();
}





void MainWindow::on_bt_1_clicked()
{
      system("beep");

}

void MainWindow::on_bt_2_clicked()
{

      system("beep");
}

void MainWindow::on_bt_3_clicked()
{
      system("beep");
}

void MainWindow::on_bt_4_clicked()
{
      system("beep");
}

void MainWindow::on_bt_5_clicked()
{
      system("beep");
}

void MainWindow::on_bt_6_clicked()
{
      system("beep");
}
